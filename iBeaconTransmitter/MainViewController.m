//
//  MainViewController.m
//  iBeaconTransmitter
//
//  Created by kiyoaki-k on 2015/03/05.
//  Copyright (c) 2015年 Kiyoaki Komai. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

// これ書いておくとちょっと楽
@synthesize region = _region;
@synthesize proximityUUID = _proximityUUID;
@synthesize peripheralManager = _peripheralManager;
@synthesize uuidLabel = _uuidLabel;
@synthesize majorText = _majorText;
@synthesize minorText = _minorText;
@synthesize transmitterSwitch = _transmitterSwitch;
@synthesize currentState = _currentState;
@synthesize countPoint = _countPoint;
@synthesize currentTimestamp = _currentTimestamp;
@synthesize loggingButton = _loggingButton;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [self initialize];
}

- (void)initialize
{
    // uuidgenで生成したUUIDからNSUUIDを生成
    _proximityUUID = [[NSUUID alloc] initWithUUIDString:iPhoneUUID];
    
    // peripheralManagerの初期化. Delegateにselfを設定し, 起動時にBluetoothがOffならばアラートを表示する
    _peripheralManager = [[CBPeripheralManager alloc]
                          initWithDelegate : self
                          queue:nil
                          options : @{CBPeripheralManagerOptionShowPowerAlertKey : @YES} ];
    
    _loggingButton.layer.borderWidth = 1.0f;
    _loggingButton.layer.cornerRadius = 10.0f;
    [[_loggingButton layer] setBorderColor:[[UIColor blackColor]CGColor]];
    
    
    /* --- カスタムボタンの生成ここから --- */
    
    // ツールバーの作成
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame : CGRectMake(0, 0, 320, 40)];
    // スタイルの設定
    toolBar.barStyle = UIBarStyleDefault;
    // ツールバーを画面サイズに合わせる
    [toolBar sizeToFit];
    // 完了ボタンを右端に配置するためのフレキシブルなスペースを作成する
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem : UIBarButtonSystemItemFlexibleSpace
                               target : self
                               action : nil];
    // 完了ボタンの作成
    UIBarButtonItem *commitButton = [[UIBarButtonItem alloc]
                                     initWithBarButtonSystemItem : UIBarButtonSystemItemDone
                                     target : self
                                     action : @selector(closeKeyboard:)];
    // ボタンをToolBarに設定
    NSArray *toolBarItems = [NSArray arrayWithObjects : spacer, commitButton, nil];
    
    // 表示, 非表示の設定
    [toolBar setItems : toolBarItems animated : YES];
    // ToolBarをTextFieldのinputAccessoryViewに追加
    self.majorText.inputAccessoryView = toolBar;
    self.minorText.inputAccessoryView = toolBar;
    
    /* --- カスタムボタンの生成ここまで --- */
    
    /* --- タイムスタンプのログを出力するための準備ここから --- */
    // 可変配列を確保
    timestampData = [NSMutableArray array];
    // CSVのヘッダを指定
    csvStr = @"Current Time,elapsed time,current state";
    // 可変配列にオブジェクトを追加
    [timestampData addObject:csvStr];
    
    count = 0;
    
    format = [[NSDateFormatter alloc] init];
    dateFormatter = @"yyMMddHHmmssSSS";
    [format setTimeZone:[NSTimeZone timeZoneWithAbbreviation: @"JST"]];
    [format setDateFormat: dateFormatter];
    
    documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentDirectory = [documentDirectories lastObject];
    NSLog(@"Document directory : %@", documentDirectory);
    
    
    /* --- タイムスタンプのログを出力するための準備ここから --- */
    
    // , majorText, minorTextのTextFieldに値をセット
    _uuidLabel.text = iPhoneUUID;
    _majorText.text = @"1";
    _minorText.text = @"1";

}

// プロトコルの中のメソッドperipheralManagerDidUpdateStateが実装されて無いと警告を受けるので, 作成する必要がある
// 必要に応じて中身は追加
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    // BluetoothがOffならリターン
    if(peripheral.state != CBPeripheralManagerStatePoweredOn){
        return;
    }
    
}

// ツールバーに追加した完了ボタンが押された時の処理
- (void)closeKeyboard:(id) sender
{
    if([_majorText.text isEqual:@""]){
        _majorText.text = @"1";
    }
    else if([_minorText.text isEqual: @""]){
        _minorText.text = @"1";
    }
    
    // 編集終了
    [self.view endEditing:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)switchStartBeacon:(id)sender {
    
    // switchがOnになった場合の処理
    if(_transmitterSwitch.on){
        NSLog(@"Start Beacon");
        
        // CLBeaconRegionを作成してアドバタイズ(発信)するデータを取得
        CLBeaconRegion *region = [[CLBeaconRegion alloc]
                                  initWithProximityUUID : _proximityUUID
                                  major : [_majorText.text intValue]
                                  minor : [_minorText.text intValue]
                                  identifier : @"jp.naist.ubilab"];
        
        // NSDictionary : オブジェクトとキー値を対として保持する配列クラス
        NSDictionary *peripheralData = [region peripheralDataWithMeasuredPower:nil];
        
        // アドバタイズ開始
        [_peripheralManager startAdvertising : peripheralData];
        
        startTime = [NSDate date];
        currentTime = [NSDate date];
        elapsedTime = [startTime timeIntervalSinceDate:currentTime];
        
        startTimeStr = [format stringFromDate: startTime];
        currentTimeStr = [format stringFromDate: currentTime];
        
        count = 0;
        
        stateStr = [NSString stringWithFormat:@"start advertise"];
        countStr = [NSString stringWithFormat:@"current point :  %d", count];

        _countPoint.text = countStr;
        _currentState.text = stateStr;
        _currentTimestamp.text = [NSString stringWithFormat:@"%@", startTimeStr];
        
        
        filePath = [documentDirectory stringByAppendingPathComponent:@"log_"];
        filePath = [filePath stringByAppendingString: startTimeStr];
        filePath = [filePath stringByAppendingString:@".csv"];
        NSLog(@"filepath : %@", filePath);
        
        fileManager = [NSFileManager defaultManager];
        
        if(![fileManager fileExistsAtPath: filePath]){
            BOOL result = [fileManager createFileAtPath : filePath contents : [NSData data] attributes : nil];
            if(!result){
                NSLog(@"ファイル作成失敗");
            }
        }
        
        fileHandle = [NSFileHandle fileHandleForWritingAtPath : filePath];
        if (!fileHandle){
            NSLog(@"ファイルハンドル作成失敗");
        }
        
        
        csvStr = [NSString stringWithFormat:@"%@,%f,%@", startTimeStr, -elapsedTime, stateStr];
        [timestampData addObject:csvStr];
        
    }
    // switchがOFFになった場合の処理
    else{
        NSLog(@"stop Beacon");
        // アドバタイズ終了
        [_peripheralManager stopAdvertising];
        
        currentTime = [NSDate date];
        elapsedTime = [startTime timeIntervalSinceDate:currentTime];
        currentTimeStr = [format stringFromDate: currentTime];
        count = 0;
        
        stateStr = [NSString stringWithFormat:@"stop advertise"];
        NSLog(@"%@",stateStr);
        _currentState.text = stateStr;
        _currentTimestamp.text = [NSString stringWithFormat:@"%@", currentTimeStr];
        
        csvStr = [NSString stringWithFormat:@"%@,%f,%@", currentTimeStr, -elapsedTime, stateStr];
        [timestampData addObject:csvStr];
        
        fullstring = [timestampData componentsJoinedByString:@"\n"];
        [fullstring writeToFile: filePath atomically: YES encoding:NSUTF8StringEncoding error:nil];
        
        arraySize = [timestampData count];
        [timestampData removeObjectsInRange: NSMakeRange(1, arraySize-1)];
        
        
    }
}

- (IBAction)majorEditBegin:(id)sender {
    // MajorのTextFieldに何かしらの値が入っていた場合
    if(![_majorText.text isEqual: @""] )
        _majorText.text = @"";
    
}

- (IBAction)minorEditBegin:(id)sender {
    // MinorのTextFieldに何かしらの値が入っていた場合
    if(![_minorText.text isEqual: @""])
        _minorText.text = nil;
}

- (IBAction)loggingTimestamp:(id)sender {
    if(_transmitterSwitch.on){
        currentTime = [NSDate date];
        elapsedTime = [startTime timeIntervalSinceDate:currentTime];
        currentTimeStr = [format stringFromDate: currentTime];
        
        count = count + 1;
        countStr = [NSString stringWithFormat:@"current point : %d", count];
        stateStr = [NSString stringWithFormat:@"logging"];
        
        NSLog(@"%@",countStr);
        NSLog(@"%@",stateStr);
        
        _countPoint.text = countStr;
        _currentState.text = stateStr;
        _currentTimestamp.text = [NSString stringWithFormat:@"%@", currentTimeStr];
        
        csvStr = [NSString stringWithFormat:@"%@,%f,%@", currentTimeStr, -elapsedTime, stateStr];
        [timestampData addObject:csvStr];
    }
    
}


// メモリの開放
- (void)dealloc
{
    // アドバタイズ終了
    [_peripheralManager stopAdvertising];
}
@end
