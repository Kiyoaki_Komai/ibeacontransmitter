//
//  MainViewController.h
//  iBeaconTransmitter
//
//  Created by kiyoaki-k on 2015/03/05.
//  Copyright (c) 2015年 Kiyoaki Komai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>

// iPhoneのUUIDをuuidgenで生成
#define iPhoneUUID  @"7092E268-C5E9-4DA5-947D-25F579DA592D"

@interface MainViewController : UIViewController <CBPeripheralManagerDelegate>
{
    NSFileHandle *fileHandle;
    NSFileManager *fileManager;
    NSMutableArray *timestampData;
    NSDate *startTime;
    NSDate *currentTime;
    NSString *startTimeStr;
    NSString *currentTimeStr;
    NSTimeInterval elapsedTime;
    NSDateFormatter *format;
    NSString *dateFormatter;
    NSString *stateStr;
    NSString *csvStr;
    
    NSInteger arraySize;
    
    
    NSInteger count;
    NSString *countStr;
    NSString *filePath;
    NSString *fullstring;
    
    NSArray *documentDirectories;
    NSString *documentDirectory;
}

@property (nonatomic) CLBeaconRegion *region;
@property (nonatomic) CBPeripheralManager *peripheralManager;
@property (nonatomic) NSUUID *proximityUUID;

@property (weak, nonatomic) IBOutlet UILabel *uuidLabel;
@property (weak, nonatomic) IBOutlet UITextField *majorText;
@property (weak, nonatomic) IBOutlet UITextField *minorText;
@property (weak, nonatomic) IBOutlet UISwitch *transmitterSwitch;
@property (weak, nonatomic) IBOutlet UILabel *currentState;
@property (weak, nonatomic) IBOutlet UILabel *currentTimestamp;
@property (weak, nonatomic) IBOutlet UILabel *countPoint;
@property (weak, nonatomic) IBOutlet UIButton *loggingButton;


- (IBAction)switchStartBeacon:(id)sender;
- (IBAction)majorEditBegin:(id)sender;
- (IBAction)minorEditBegin:(id)sender;
- (IBAction)loggingTimestamp:(id)sender;


@end

